﻿namespace BinaryTask1.Models
{
    public struct ProjectStruct
    {
        public Project Project { get; set; }
        public BinaryTask1.Models.Task LongestTask { get; set; }
        public BinaryTask1.Models.Task ShortestTask { get; set; }
        public int UserCount { get; set; }

        public override string ToString()
        {
            return $"{Project}\n{LongestTask}\n{ShortestTask}\n{UserCount}";
        }
    }

}