﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using BinaryTask1.Services;
using Newtonsoft.Json;

namespace BinaryTask1
{
    class Program
    {
        private static readonly HttpClient httpClient = new HttpClient();
        
        
        static void Main(string[] args)
        {
            HttpService httpService = new HttpService(httpClient);
            LectureService lectureService = new LectureService(httpService);

            lectureService.SendTestData();

            bool isWorking = true;

            string menu = "1 - Project-Task_Count by user's id;\n" +
                          "2 - Tasks by user's id with task's name < 45;\n" +
                          "3 - Tasks finished in 2021 by user's id;\n" +
                          "4 - Teams with more than 10 y.o. participants;\n" +
                          "5 - Users with tasks;\n" +
                          "6 - User structure by id;\n" +
                          "7 - Project structure;\n" +
                          "0 - Exit;";


            while (isWorking)
            {
                Console.WriteLine(menu);
                string input = Console.ReadLine();

                switch (input)
                {
                    case "0":
                    {
                        isWorking = false;
                        break;
                    }
                    case "1":
                    {
                        Console.WriteLine("User's id (try 0): ");
                        string id = Console.ReadLine();

                        int userIdQuery1;
                        if (Int32.TryParse(id, out userIdQuery1))
                        {
                            var res = lectureService.GetProjectTaskCountByUser(userIdQuery1);
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            foreach (var item in res)
                            {
                                Console.WriteLine(item.Key + "\t" + item.Value);
                            }

                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        break;
                    }
                    case "2":
                    {
                        Console.WriteLine("User's id (try 0): ");
                        string id = Console.ReadLine();

                        int userIdQuery2;
                        if (Int32.TryParse(id, out userIdQuery2))
                        { 
                            var res2 = lectureService.GetTasksByUserLess45(userIdQuery2);
                            Console.ForegroundColor = ConsoleColor.Blue;
                            foreach (var item in res2)
                            {
                                Console.WriteLine(item);
                            }

                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        break;
                    }
                    case "3":
                    {
                        Console.WriteLine("User's id (try 0): ");
                        string id = Console.ReadLine();

                        int userIdQuery3;
                        if (Int32.TryParse(id, out userIdQuery3))
                        {
                            var res3 = lectureService.GetTasksByUserDone(userIdQuery3);
                            Console.ForegroundColor = ConsoleColor.DarkCyan;
                            foreach (var item in res3)
                            {
                                Console.WriteLine(item.Item1 + "\t" + item.Item2);
                            }

                            Console.ForegroundColor = ConsoleColor.White;
                        }

                        break;
                    }
                    case "4":
                    {
                        var res4 = lectureService.GetTeamsWithUsersOlder10();
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        foreach (var item in res4)
                        {
                            Console.WriteLine(item.Key + "\t\n");
                            foreach (var user in item.Value)
                            {
                                Console.WriteLine("\t" + user);
                            }
                            Console.WriteLine();
                        }

                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    }
                    case "5":
                    {
                        var res5 = lectureService.GetUsersWithTasks();
                        Console.ForegroundColor = ConsoleColor.Green;
                        foreach (var item in res5)
                        {
                            Console.WriteLine(item.Key);
                            foreach (var userTask in item.Value)
                            {
                                Console.WriteLine("\t" + userTask);
                            }
                        }

                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    }
                    case "6":
                    {
                        Console.WriteLine("User's id (try 0): ");
                        string id = Console.ReadLine();

                        int userIdQuery6;
                        if (Int32.TryParse(id, out userIdQuery6))
                        {
                            var res6 = lectureService.GetUserStruct(userIdQuery6);
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine(res6.User);
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                        break;
                    }
                    case "7":
                    {
                        var res7 = lectureService.GetProjectStruct();
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        foreach (var item in res7)
                        {
                            Console.WriteLine(item + "\n");
                        }
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    }
                    default: continue;
                }
            }

            
        }

    }
}
