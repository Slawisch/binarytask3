﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Teams")]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;
        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public IEnumerable<TeamDTO> GetTeams()
        {
            return _teamService.GetTeams();
        }

        [HttpGet("{id}")]
        public IActionResult GetTeam(int id)
        {
            try
            {
                return new JsonResult(_teamService.GetTeam(id));
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }

        }

        [HttpPost]
        public IActionResult AddTeam(TeamDTO team)
        {
            try
            {
                _teamService.CreateTeam(team);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new ConflictResult();
            }

        }

        [HttpDelete]
        public IActionResult DeleteTeam(int id)
        {
            try
            {
                _teamService.DeleteTeam(id);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }

        }

        [HttpPut]
        public IActionResult UpdateTeam(int id, TeamDTO team)
        {
            try
            {
                _teamService.UpdateTeam(id, team);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }

        }
    }
}
