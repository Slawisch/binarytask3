﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;

namespace BinaryTask3.WebAPI.Controllers
{
    [ApiController]
    [Route("api/Projects")]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;
        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public IEnumerable<ProjectDTO> GetProjects()
        {
            return _projectService.GetProjects();
        }

        [HttpGet("{id}")]
        public IActionResult GetProject(int id)
        {
            try
            {
                return new JsonResult(_projectService.GetProject(id));
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }
            
        }

        [HttpPost]
        public IActionResult AddProject(ProjectDTO project)
        {
            try
            {
                _projectService.CreateProject(project);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new ConflictResult();
            }
            
        }

        [HttpDelete]
        public IActionResult DeleteProject(int id)
        {
            try
            {
                _projectService.DeleteProject(id);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }
            
        }

        [HttpPut]
        public IActionResult UpdateProject(int id, ProjectDTO project)
        {
            try
            {
                _projectService.UpdateProject(id, project);
                return new OkResult();
            }
            catch (ArgumentException)
            {
                return new NotFoundResult();
            }
            
        }

    }
}
