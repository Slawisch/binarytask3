﻿
namespace BinaryTask3.DAL.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
