﻿using System;

#nullable enable

namespace BinaryTask3.DAL.Entities
{
    public class Team : BaseEntity
    {
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
