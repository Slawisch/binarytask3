﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinaryTask3.DAL.Entities;

namespace BinaryTask3.DAL.Repositories
{
    public abstract class BaseRepository<T> where T: BaseEntity
    {
        private static readonly List<T> Entities = new List<T>();
        public IEnumerable<T> Read()
        {
            return Entities;
        }

        public T Read(int id)
        {
            try
            {
                return Entities.Find(e => e.Id == id);
            }
            catch
            {
                throw new ArgumentException($"Entity with id({id}) not found");
            }
        }

        public void Update(int id, T entity)
        {
            try
            {
                Delete(id);
                Create(entity);
            }
            catch
            {
                throw;
            }

        }

        public void Create(T entity)
        {
            if (Entities.Exists(e => e.Id == entity.Id))
                throw new ArgumentException($"Entity with id({entity.Id}) already exists");

            Entities.Add(entity);
        }

        public void Delete(int id)
        {
            if (Entities.RemoveAll(entity => entity.Id == id) == 0)
                throw new ArgumentException($"Entity with id({id}) not found");
        }
    }
}
