﻿using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.DAL.Repositories
{
    class UserRepository : BaseRepository<User>, IRepository<User>
    {
    }
}
