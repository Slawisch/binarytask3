﻿using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProjectRepository _projectRepository;
        private TaskRepository _taskRepository;
        private TeamRepository _teamRepository;
        private UserRepository _userRepository;

        public UnitOfWork()
        {

        }

        public IRepository<Project> Projects
        {
            get
            {
                if (_projectRepository == null)
                    _projectRepository = new ProjectRepository();
                return _projectRepository;
            }
        }

        public IRepository<TaskEntity> Tasks
        {
            get
            {
                if (_taskRepository == null)
                    _taskRepository = new TaskRepository();
                return _taskRepository;
            }
        }

        public IRepository<Team> Teams
        {
            get
            {
                if (_teamRepository == null)
                    _teamRepository = new TeamRepository();
                return _teamRepository;
            }
        }

        public IRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository();
                return _userRepository;
            }
        }
    }
}
