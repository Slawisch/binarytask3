﻿using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.DAL.Repositories
{
    class TeamRepository : BaseRepository<Team>, IRepository<Team>
    {
    }
}
