﻿using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.DAL.Repositories
{
    public class ProjectRepository : BaseRepository<Project>, IRepository<Project>
    {
       
    }
}
