﻿using System.Threading.Tasks;
using BinaryTask3.DAL.Entities;

namespace BinaryTask3.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Project> Projects { get; }
        IRepository<TaskEntity> Tasks { get; }
        IRepository<Team> Teams { get; }
        IRepository<User> Users { get; }

    }
}
