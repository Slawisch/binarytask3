﻿using System.Collections.Generic;

namespace BinaryTask3.DAL.Interfaces
{
    public interface IRepository<T> where T: class
    {
        IEnumerable<T> Read();
        T Read(int id);
        void Update(int id, T task);
        void Create(T task);
        void Delete(int id);
    }
}
