﻿using System.Collections.Generic;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) {}


        public IEnumerable<ProjectDTO> GetProjects()
        {
            return Mapper.Map<IEnumerable<ProjectDTO>>(UnitOfWork.Projects.Read());
        }

        public ProjectDTO GetProject(int id)
        {
            try
            {
                return Mapper.Map<ProjectDTO>(UnitOfWork.Projects.Read(id));
            }
            catch
            {
                throw;
            }
        }

        public void UpdateProject(int id, ProjectDTO project)
        {
            try
            {
                UnitOfWork.Projects.Update(id, Mapper.Map<Project>(project));
            }
            catch
            {
                throw;
            }
        }

        public void CreateProject(ProjectDTO project)
        {
            try
            {
                UnitOfWork.Projects.Create(Mapper.Map<Project>(project));
            }
            catch
            {
                throw;
            }
        }

        public void DeleteProject(int id)
        {
            try
            {
                UnitOfWork.Projects.Delete(id);
            }
            catch
            {
                throw;
            }

        }

    }
}