﻿using System.Collections.Generic;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public class TaskService : BaseService, ITaskService
    {
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public IEnumerable<TaskDTO> GetTasks()
        {
            return Mapper.Map<IEnumerable<TaskDTO>>(UnitOfWork.Tasks.Read());
        }

        public TaskDTO GetTask(int id)
        {
            try
            {
                return Mapper.Map<TaskDTO>(UnitOfWork.Tasks.Read(id));
            }
            catch
            {
                throw;
            }
        }

        public void UpdateTask(int id, TaskDTO task)
        {
            try
            {
                UnitOfWork.Tasks.Update(id, Mapper.Map<TaskEntity>(task));
            }
            catch
            {
                throw;
            }
        }

        public void CreateTask(TaskDTO task)
        {
            try
            {
                UnitOfWork.Tasks.Create(Mapper.Map<TaskEntity>(task));
            }
            catch
            {
                throw;
            }
        }

        public void DeleteTask(int id)
        {
            try
            {
                UnitOfWork.Tasks.Delete(id);
            }
            catch
            {
                throw;
            }
        }
    }
}
