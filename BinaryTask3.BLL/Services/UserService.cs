﻿using System.Collections.Generic;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            return Mapper.Map<IEnumerable<UserDTO>>(UnitOfWork.Users.Read());
        }

        public UserDTO GetUser(int id)
        {
            try
            {
                return Mapper.Map<UserDTO>(UnitOfWork.Users.Read(id));
            }
            catch
            {
                throw;
            }
        }

        public void UpdateUser(int id, UserDTO user)
        {
            try
            {
                UnitOfWork.Users.Update(id, Mapper.Map<User>(user));
            }
            catch
            {
                throw;
            }
        }

        public void CreateUser(UserDTO user)
        {
            try
            {
                UnitOfWork.Users.Create(Mapper.Map<User>(user));
            }
            catch
            {
                throw;
            }
        }

        public void DeleteUser(int id)
        {
            try
            {
                UnitOfWork.Users.Delete(id);
            }
            catch
            {
                throw;
            }
        }
    }
}
