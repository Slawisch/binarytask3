﻿using System.Collections.Generic;
using AutoMapper;
using BinaryTask3.BLL.Interfaces;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;
using BinaryTask3.DAL.Interfaces;

namespace BinaryTask3.BLL.Services
{
    public class TeamService : BaseService, ITeamService
    {
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
        }

        public IEnumerable<TeamDTO> GetTeams()
        {
            return Mapper.Map<IEnumerable<TeamDTO>>(UnitOfWork.Teams.Read());
        }

        public TeamDTO GetTeam(int id)
        {
            try
            {
                return Mapper.Map<TeamDTO>(UnitOfWork.Teams.Read(id));
            }
            catch
            {
                throw;
            }
        }

        public void UpdateTeam(int id, TeamDTO team)
        {
            try
            {
                UnitOfWork.Teams.Update(id, Mapper.Map<Team>(team));
            }
            catch
            {
                throw;
            }
        }

        public void CreateTeam(TeamDTO team)
        {
            try
            {
                UnitOfWork.Teams.Create(Mapper.Map<Team>(team));
            }
            catch
            {
                throw;
            }
        }

        public void DeleteTeam(int id)
        {
            try
            {
                UnitOfWork.Teams.Delete(id);
            }
            catch
            {
                throw;
            }
        }
    }
}
