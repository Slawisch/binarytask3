﻿using AutoMapper;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;

namespace BinaryTask3.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<TeamDTO, Team>();
        }
    }
}
