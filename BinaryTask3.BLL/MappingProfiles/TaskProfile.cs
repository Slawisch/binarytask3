﻿using AutoMapper;
using BinaryTask3.Common.DTOs;
using BinaryTask3.DAL.Entities;

namespace BinaryTask3.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskEntity, TaskDTO>();
            CreateMap<TaskDTO, TaskEntity>();
        }
    }
}
